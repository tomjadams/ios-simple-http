#import <XCTest/XCTest.h>
#import "UrlEncoding.h"
#import "Http.h"

@interface NSString (Http)
- (BOOL)contains:(NSString *)other;
@end

@implementation NSString (Http)

- (BOOL)contains:(NSString *)other {
    return [self rangeOfString:other].length > 0;
}

@end

@interface HttpTests : XCTestCase
@end

@implementation HttpTests

- (void)testGoogleSearch {
    NSString *queryString = [UrlEncoding queryStringFromParams:@{@"q" : @"cogent melbourne", @"rls=en" : @"en", @"ie" : @"UTF-8", @"oe" : @"UTF-8"}];
    NSURL *url = [NSURL URLWithString:SWF(@"https://www.google.com.au/search?%@", queryString)];
    [Http getToUrl:url
           success:^(NSData *data) {
               NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
               XCTAssertTrue([response contains:@"creates useful software that gets results"]);
           }
           failure:^(NSURLResponse *response, NSError *error) {
               XCTFail(@"Expected success");
           }];
}

- (void)testPostEcho {

    NSString *payload = @"{\"foo\":\"bar\", \"baz\":1}";
    NSString *queryString = [UrlEncoding queryStringFromParams:@{@"user[email]" : @"test+123@test.com", @"user[password]" : @"1p@$$w0rd"}];
    NSURL *url = [NSURL URLWithString:SWF(@"http://httpbin.org/post?%@", queryString)];
    [Http postToUrl:url payload:payload mimeType:@"application/json"
            success:^(NSData *data) {
                NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                XCTAssertTrue([response contains:@"\"user[email]\": \"test+123@test.com\""]);
                XCTAssertTrue([response contains:@"\"user[password]\": \"1p@$$w0rd\""]);
                XCTAssertTrue([response contains:@"\"data\": \"{\\\"foo\\\":\\\"bar\\\", \\\"baz\\\":1}\""]);
                XCTAssertTrue([response contains:@"\"Content-Type\": \"application/json\""]);
            } failure:^(NSURLResponse *response, NSError *error) {
                XCTFail(@"Expected success");
            }];
}

@end
