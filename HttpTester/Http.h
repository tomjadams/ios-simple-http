#import <Foundation/Foundation.h>

#define SWF(a, args...) [NSString stringWithFormat:a, args]

#define HTTP_OK 200
#define HTTP_CREATED 200

@interface Http : NSObject

+ (void)getToUrl:(NSURL *)url success:(void (^)(NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure;

+ (void)postToUrl:(NSURL *)url payload:(NSString *)payload mimeType:(NSString *)mimeType success:(void (^)(NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure;

+ (NSInteger)statusCodeForResponse:(NSURLResponse *)response;

+ (NSString *)stringForStatusCodeForResponse:(NSURLResponse *)response;

@end
