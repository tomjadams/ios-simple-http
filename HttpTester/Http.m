#import "Http.h"

@implementation Http

static NSTimeInterval HTTPRequestTimeoutInterval = 60.0;
static NSInteger HTTPDefaultStatusCode = 0;

#pragma mark - Public API

+ (void)getToUrl:(NSURL *)url success:(void (^)(NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure {
    [self sendSynchronousRequest:[self baseRequestWithUrl:url] success:success failure:failure];
}

+ (void)postToUrl:(NSURL *)url payload:(NSString *)payload mimeType:(NSString *)mimeType success:(void (^)(NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure {
    [self sendSynchronousRequest:[self postRequestWithUrl:url payload:payload mimeType:mimeType] success:success failure:failure];
}

+ (NSInteger)statusCodeForResponse:(NSURLResponse *)response {
    return response != nil && [response isKindOfClass:[NSHTTPURLResponse class]] ? [((NSHTTPURLResponse *) response) statusCode] : HTTPDefaultStatusCode;
}

+ (NSString *)stringForStatusCodeForResponse:(NSURLResponse *)response {
    return [NSHTTPURLResponse localizedStringForStatusCode:[self statusCodeForResponse:response]];
}

#pragma mark - Private methods

+ (void)sendSynchronousRequest:(NSURLRequest *)request success:(void (^)(NSData *))success failure:(void (^)(NSURLResponse *, NSError *))failure {
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *content = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error == nil && ([self statusCodeForResponse:response] == HTTP_OK || [self statusCodeForResponse:response] == HTTP_CREATED)) {
        success(content);
    } else {
        failure(response, error);
    }
}

+ (NSURLRequest *)baseRequestWithUrl:(NSURL *)url {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
    [request setTimeoutInterval:HTTPRequestTimeoutInterval];
    [request setHTTPShouldHandleCookies:YES];
    return request;
}

+ (NSURLRequest *)postRequestWithUrl:(NSURL *)url payload:(NSString *)payload mimeType:(NSString *)mimeType {
    NSMutableURLRequest *request = (NSMutableURLRequest *) [self baseRequestWithUrl:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:mimeType forHTTPHeaderField:@"Content-Type"];
    NSData *postData = [payload dataUsingEncoding:NSUTF8StringEncoding];
    [request setValue:SWF(@"%d", [postData length]] forHTTPHeaderField:@"Content-Length");
    [request setHTTPBody:postData];
    return request;
}

@end
