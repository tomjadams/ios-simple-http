#import "UrlEncoding.h"

#define URI_RESERVED_CHARACTERS @"!*'\"();:@&=+$,/?%#[]% "

@interface NSString (Functions)
+ (NSString *(^)(NSArray *))concatF;
@end

@implementation NSString (Functions)

+ (NSString *(^)(NSArray *))concatF {
    return ^NSString *(NSArray *as) {
        NSMutableString *s = [NSMutableString string];
        for (NSString *a in as) {
            [s appendString:a];
        }
        return s;
    };
}

@end

@interface NSArray (Map)
- (NSArray *)intersperse:(id)object;

- (NSArray *)map:(id (^)(id obj))block;
@end

@implementation NSArray (Map)

- (NSArray *)map:(id (^)(id))f {
    NSMutableArray *r = [NSMutableArray arrayWithCapacity:[self count]];
    for (id item in self) {
        [r addObject:f(item)];
    }
    return [NSArray arrayWithArray:r];
}

- (NSArray *)intersperse:(id)object {
    NSMutableArray *interspersed = [NSMutableArray array];
    for (NSUInteger i = 0; i < [self count]; ++i) {
        [interspersed addObject:self[i]];
        if (i != [self count] - 1) [interspersed addObject:object];
    }
    return [NSArray arrayWithArray:interspersed];
}

- (NSString *)join:(NSString *)str {
    NSString *(^f)(NSArray *) = [NSString concatF];
    return f([self intersperse:str]);
}

@end

@implementation UrlEncoding

+ (NSString *)queryStringFromParams:(NSDictionary *)params {
    return [[[params allKeys] map:^id(id key) {
        return [NSString stringWithFormat:@"%@=%@", [self encodeString:key], [self encodeString:params[key]]];
    }] join:@"&"];
}

+ (NSString *)encodeString:(NSString *)unencoded {
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef) unencoded, NULL, (CFStringRef) URI_RESERVED_CHARACTERS, kCFStringEncodingUTF8));
}

@end
