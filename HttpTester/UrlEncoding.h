#import <Foundation/Foundation.h>

@interface UrlEncoding : NSObject

+ (NSString *)encodeString:(NSString *)unencoded;

+ (NSString *)queryStringFromParams:(NSDictionary *)params;

@end
